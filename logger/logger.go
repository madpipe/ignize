package logger

// Logger is a interface for any logging system that might be used
type Logger interface {
	Log(message string) error
}
