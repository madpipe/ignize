package drivers

import (
	"testing"
)

func TestLog(t *testing.T) {

	logger := Simple{}

	t.Run("Print", func(t *testing.T) {
		if err := logger.Log(""); err != nil {
			t.Errorf("Expected no error; got %s", err.Error())
		}
	})
}
