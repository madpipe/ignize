package drivers

import (
	"fmt"
	"testing"
)

// Simple is the most basic implementation of Logger
// It prints to STDOUT and if TestPtr is set it also prints to STDOUT durring `go test`
type Simple struct {
	TestingPtr *testing.T
}

// Log will print to stdout the received message
func (s *Simple) Log(message string) error {
	if s.TestingPtr != nil {
		s.TestingPtr.Log(message)
	}

	fmt.Println(message)

	return nil
}
