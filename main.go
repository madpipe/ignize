package main

import (
	"fmt"
	"log"
	"os"

	logger_driver "gitlab.com/ovidiub13/ignize/logger/drivers"
	storage_driver "gitlab.com/ovidiub13/ignize/storage/drivers"
	"gitlab.com/ovidiub13/ignize/usecases"
)

var logger = logger_driver.Simple{}
var storage = storage_driver.Local{}

func main() {
	picInteractor := usecases.PictureInteractor{
		Logger:  &logger,
		Storage: &storage,
	}

	location := os.Args[1]

	pictures, err := picInteractor.GetPictures(location, 10)

	if err != nil {
		log.Fatalln(err.Error())
	}

	for _, pic := range pictures {
		fmt.Printf("Found Picture %v\n", pic)
	}
}
