package storage

import "gitlab.com/ovidiub13/ignize/entities"

// Storage is the interface for the Storage adapter to use
type Storage interface {
	// GetPictures returns a list of _Picture_s from the files found in storage
	GetPictures(location string, count int) ([]entities.Picture, error)
	// GetPicture returns a _Picture_ that matches _name_
	GetPicture(location, name string) (entities.Picture, error)
}
