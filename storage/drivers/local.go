package drivers

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/ovidiub13/ignize/logger"

	entities "gitlab.com/ovidiub13/ignize/entities"
)

// Local is a `Storage` implementation for the local filesystem
type Local struct {
	rootDir string
	logger  logger.Logger
}

// NewLocalStorage is a constructor for Local
func NewLocalStorage(rootDir string, logger logger.Logger) (*Local, error) {
	l := new(Local)
	l.logger = logger
	absPath, err := filepath.Abs(rootDir)
	if err != nil {
		message := fmt.Sprintf("Failed to get absolute path of: \"%s\"", rootDir)
		l.logger.Log(message)
		return nil, err
	}
	l.rootDir = absPath

	if !l.isLocationValid(absPath) {
		message := fmt.Sprintf("Root location \"%s\" is not a valid location.", rootDir)
		l.logger.Log(message)
		return nil, err
	}

	return l, nil
}

// GetPictures returns a list of _Picture_s from the files found in storage
func (l *Local) GetPictures(location string, count int) ([]entities.Picture, error) {
	return []entities.Picture{}, nil
}

// GetPicture returns a _Picture_ that matches _name_
func (l *Local) GetPicture(location, name string) (entities.Picture, error) {
	return entities.Picture{}, nil
}

func (l *Local) isLocationValid(path string) bool {
	// Check if path is under allowed root
	absPath, err := filepath.Abs(path)
	if err != nil {
		message := fmt.Sprintf("Failed to get absolute path of \"%s\"", path)
		l.logger.Log(message)
		return false
	}
	if !strings.HasPrefix(absPath, l.rootDir) {
		message := fmt.Sprintf("Path \"%s\" is not a subdirectory of \"%s\"", path, l.rootDir)
		l.logger.Log(message)
		return false
	}
	// Check if path is a directory
	fi, err := os.Stat(path)
	if err != nil {
		l.logger.Log(err.Error())
		return false
	}
	if !fi.Mode().IsDir() {
		message := fmt.Sprintf("Path \"%s\" does not point to a directory", path)
		l.logger.Log(message)
		return false
	}
	// Check if path can be read from
	_, err = ioutil.ReadDir(path)
	if err != nil {
		message := fmt.Sprintf("Directory \"%s\" cannot be opened or read", path)
		l.logger.Log(message)
		return false
	}

	return true
}

func (l *Local) fileIsPicture(filePath string) bool {
	contentType := l.getContentType(filePath)
	if strings.HasPrefix(contentType, "image/") {
		return true
	}
	return false
}

func (l *Local) getContentType(filePath string) string {
	f, err := os.Open(filePath)
	if err != nil {
		message := fmt.Sprintf("Error opening file \"%s\": %s", filePath, err.Error())
		l.logger.Log(message)
		return ""
	}
	defer f.Close()

	buffer := make([]byte, 512)
	_, err = f.Read(buffer)
	if err != nil {
		message := fmt.Sprintf("Error reading from file \"%s\": %s", filePath, err.Error())
		l.logger.Log(message)
		return ""
	}

	contentType := http.DetectContentType(buffer)

	return contentType
}
