package drivers

import (
	"os"
	"path"
	"regexp"
	"testing"

	logDrivers "gitlab.com/ovidiub13/ignize/logger/drivers"
)

var testFilesLocation = "../../test_assets"

func TestGetPictures(t *testing.T) {
	storage, _ := NewLocalStorage(testFilesLocation, &logDrivers.Simple{TestingPtr: t})

	t.Run("Location Error", func(t *testing.T) {
		invalidLocation := "doesNotExist"
		_, err := storage.GetPictures(invalidLocation, 10)
		if err == nil {
			t.Errorf("Expected Invalid location error. Got nil")
		}
	})
	t.Run("Got pictures", func(t *testing.T) {
		pictures, err := storage.GetPictures("", 10)
		if err != nil {
			t.Errorf("Expected success. Got error: %s", err.Error())
		}
		if len(pictures) <= 0 || len(pictures) > 10 {
			t.Errorf("Expected between 1 and 10 pictures. Got: %d", len(pictures))
		}
	})
	t.Run("Got no pictures", func(t *testing.T) {
		pictures, err := storage.GetPictures("empty_dir", 10)
		if err != nil {
			t.Errorf("Expected success. Got error: %s", err.Error())
		}
		if len(pictures) != 0 {
			t.Errorf("Expected 0 pictures. Got: %d", len(pictures))
		}
	})
}

func TestGetPicture(t *testing.T) {
	storage, _ := NewLocalStorage(testFilesLocation, &logDrivers.Simple{TestingPtr: t})

	pictureFileName := "beautiful-environment-idyllic-2067659.jpg"
	pictureLocation := ""

	t.Run("Location Error", func(t *testing.T) {
		invalidLocation := "doesNotExist"
		_, err := storage.GetPicture(invalidLocation, pictureFileName)
		if err == nil {
			t.Errorf("Expected Invalid location error. Got nil")
		}
	})
	t.Run("Got picture", func(t *testing.T) {
		picture, err := storage.GetPicture(pictureLocation, pictureFileName)
		if err != nil {
			t.Errorf("Expected success. Got error: %s", err.Error())
		}
		if picture.Location != path.Join(storage.rootDir, pictureLocation) {
			t.Errorf("Expected picture location \"%s\". Got \"%s\"",
				path.Join(storage.rootDir, pictureLocation), picture.Location)
		}
		if picture.Name != pictureFileName {
			t.Errorf("Expected picture name \"%s\". got \"%s\"", pictureFileName, picture.Name)
		}
	})
	t.Run("Picture does not exist", func(t *testing.T) {
		invalidFileName := "doesNotExist.jpg"
		_, err := storage.GetPicture(pictureLocation, invalidFileName)
		if err == nil {
			t.Error("Expected Picture does not exist error. Got nil")
		}
	})
}

func TestIsLocationValid(t *testing.T) {
	storage, _ := NewLocalStorage(testFilesLocation, &logDrivers.Simple{TestingPtr: t})

	t.Run("Location not a directory", func(t *testing.T) {
		location := path.Join(storage.rootDir, "aeroplane-aircraft-airplane-1154619.jpg")
		if storage.isLocationValid(location) {
			t.Error("Expected invalid location.")
		}
	})
	t.Run("Location does not exist", func(t *testing.T) {
		location := path.Join(storage.rootDir, "doesNotExist")
		if storage.isLocationValid(location) {
			t.Error("Expected invalid location.")
		}
	})
	t.Run("Location no read access", func(t *testing.T) {
		location := path.Join(storage.rootDir, "no_read_access")

		// Limiting permissions for test
		err := os.Chmod(location, 0000)
		if err != nil {
			t.Fatalf("Failed to change permissions to file \"%s\"", location)
		}

		if storage.isLocationValid(location) {
			t.Error("Expected invalid location.")
		}

		// Resttoring permissions for git
		err = os.Chmod(location, 0000)
		if err != nil {
			t.Fatalf("Failed to change permissions to file \"%s\"", location)
		}
	})
	t.Run("Location above storage root", func(t *testing.T) {
		location := path.Join(storage.rootDir, "..")
		if storage.isLocationValid(location) {
			t.Error("Expected invalid location.")
		}
	})
	t.Run("Location is good", func(t *testing.T) {
		location := storage.rootDir
		if !storage.isLocationValid(location) {
			t.Error("Expected valid location.")
		}
	})
}

func TestFileIsPicture(t *testing.T) {
	storage, _ := NewLocalStorage(testFilesLocation, &logDrivers.Simple{TestingPtr: t})

	t.Run("True", func(t *testing.T) {
		filePath := path.Join(storage.rootDir, "beautiful-environment-idyllic-2067659.jpg")
		if !storage.fileIsPicture(filePath) {
			t.Errorf("File \"%s\" expected to be picture", filePath)
		}
	})
	t.Run("False", func(t *testing.T) {
		filePath := path.Join(storage.rootDir, "not_a_picture.txt")
		if storage.fileIsPicture(filePath) {
			t.Errorf("File \"%s\" expected not to be a picture", filePath)
		}
	})
}

func isContentType(value string) bool {
	rxPattern := regexp.MustCompile("^[a-z-]+/[a-z-]+")
	if !rxPattern.MatchString(value) {
		return false
	}
	return true
}

func TestGetContentType(t *testing.T) {
	storage, _ := NewLocalStorage(testFilesLocation, &logDrivers.Simple{TestingPtr: t})

	t.Run("OK", func(t *testing.T) {
		filePath := path.Join(storage.rootDir, "african-penguin-animal-animal-photography-2078475.jpg")
		contentType := storage.getContentType(filePath)
		if !isContentType(contentType) {
			t.Errorf("Expected a content type string. Got: %s", contentType)
		}
	})
	t.Run("File does not exist", func(t *testing.T) {
		filePath := path.Join(storage.rootDir, "doesNotExist")
		contentType := storage.getContentType(filePath)
		if len(contentType) > 0 {
			t.Error("Expected empty ContentType")
		}
	})
	t.Run("No read access", func(t *testing.T) {
		filePath := path.Join(storage.rootDir, "no_read_access.jpg")

		// Limiting permissions for test
		err := os.Chmod(filePath, 0000)
		if err != nil {
			t.Fatalf("Failed to change permissions to file \"%s\"", filePath)
		}

		contentType := storage.getContentType(filePath)
		if len(contentType) > 0 {
			t.Error("Expected empty ContentType")
		}

		// Restoring permissions for git
		err = os.Chmod(filePath, 0664)
		if err != nil {
			t.Fatalf("Failed to change permissions to file \"%s\"", filePath)
		}
	})
}
