package usecases

import (
	"errors"
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"

	"gitlab.com/ovidiub13/ignize/entities"
	logger "gitlab.com/ovidiub13/ignize/logger/drivers"
	storage "gitlab.com/ovidiub13/ignize/storage/drivers"
)

var pic1 = entities.Picture{Location: "", Name: "Pic1"}
var pic2 = entities.Picture{Location: "dir1", Name: "Pic2"}
var pic3 = entities.Picture{Location: "dir1", Name: "Pic3"} // Not in ST

func TestGetPicture(t *testing.T) {
	stCtrl := gomock.NewController(t)
	defer stCtrl.Finish()
	mockStorage := storage.NewMockStorage(stCtrl)

	lgCtrl := gomock.NewController(t)
	defer lgCtrl.Finish()
	mockLogger := logger.NewMockLogger(lgCtrl)

	pictureInteractor := PictureInteractor{
		Storage: mockStorage,
		Logger:  mockLogger,
	}

	t.Run("Picture exists", func(t *testing.T) {
		mockStorage.EXPECT().GetPicture(pic1.Location, pic1.Name).Return(pic1, nil)

		picture, err := pictureInteractor.GetPicture(pic1.Location, pic1.Name)

		if err != nil {
			t.Errorf("Expected no error. Got: %s", err)
		}
		if !picture.SameAs(pic1) {
			t.Errorf("Expected %v; Got %v", pic1, picture)
		}

	})

	t.Run("Picture does not exist", func(t *testing.T) {
		message := fmt.Sprintf("The picture \"%s\" does not exist at location: \"%s\"",
			pic3.Name,
			pic3.Location)
		mockLogger.EXPECT().Log(message)
		mockStorage.EXPECT().
			GetPicture(pic3.Location, pic3.Name).
			Return(
				entities.Picture{},
				errors.New(message))

		_, err := pictureInteractor.GetPicture(pic3.Location, pic3.Name)

		if err == nil {
			t.Errorf("Expected error.")
		}
	})
}

func TestGetPictures(t *testing.T) {
	stCtrl := gomock.NewController(t)
	defer stCtrl.Finish()
	mockStorage := storage.NewMockStorage(stCtrl)

	lgCtrl := gomock.NewController(t)
	defer lgCtrl.Finish()
	mockLogger := logger.NewMockLogger(lgCtrl)

	pictureInteractor := PictureInteractor{
		Storage: mockStorage,
		Logger:  mockLogger,
	}

	t.Run("Pictures exist", func(t *testing.T) {
		expected := []entities.Picture{
			pic2, pic3,
		}

		mockStorage.EXPECT().
			GetPictures(pic2.Location, 10).
			Return(expected, nil)

		pictures, err := pictureInteractor.GetPictures(pic2.Location, 10)

		if err != nil {
			t.Errorf("Expected no error. Got: %s", err)
		}
		if len(pictures) != len(expected) {
			t.Errorf("Expected list of %d; Got %v", len(expected), len(pictures))
		}

		for i := 0; i < len(expected); i++ {
			if !pictures[i].SameAs(expected[i]) {
				t.Errorf("Expected pictures to match. %v does not match %v", pictures[i], expected[i])
			}
		}
	})

	t.Run("Error from storage", func(t *testing.T) {
		message := "Some Error from Storage"
		mockLogger.EXPECT().Log(message)
		mockStorage.EXPECT().
			GetPictures(pic2.Location, 10).
			Return(
				[]entities.Picture{},
				errors.New(message))

		_, err := pictureInteractor.GetPictures(pic2.Location, 10)

		if err == nil {
			t.Errorf("Expected error.")
		}
	})
}
