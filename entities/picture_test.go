package entities

import (
	"testing"
)

func TestPictureSameAs(t *testing.T) {
	pic1 := Picture{
		Location: "/photos",
		Name:     "Picture1",
	}
	pic2 := Picture{
		Location: "/photos",
		Name:     "Picture2",
	}
	t.Run("Same", func(t *testing.T) {
		if !pic1.SameAs(pic1) {
			t.Errorf("Expected \"%#v\" to be the same as \"%#v\"", pic1, pic1)
		}
	})
	t.Run("Different", func(t *testing.T) {
		if pic1.SameAs(pic2) {
			t.Errorf("Expected \"%#v\" to be different than \"%#v\".", pic1, pic2)
		}
	})
}
