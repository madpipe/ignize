# Ignize

Ignize is a photo organization tool that helps you find and sort pictures by adding tags to each picture and the use the tags to filter.

The project is meant as a learning experience where I want to learn and put in practice the [Go programming language](https://golang.org/) using [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) and [Test Driven Development](https://en.wikipedia.org/wiki/Test-driven_development).

This repository contains only the core of Ignize. Other projects will follow that will provide user interfaces for Web, Desktop and Mobile.
